/**
 * ClientLib for Component List Component
 */

$(document).ready(function(){
$("#getCompBtn").click(function(){
    $("#pageList").empty();
    var name = $("#compList").val();
	var words = name.split(" ");
    name = words.join("%20");
	$.getJSON("/bin/testProject/compListServlet?compName="+name, function(data) {
		let pageArray = data.pages;
        for(var i in pageArray) { 
            $("#pageList").append(i + ". <a href=\""+pageArray[i].path+".html\">"+pageArray[i].name+"</a>");
            $("#pageList").append("<br><br>");
        }
	});
});
});