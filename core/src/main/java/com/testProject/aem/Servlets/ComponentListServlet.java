package com.testProject.aem.Servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import com.testProject.aem.Services.ComponentListUtil;


@SlingServlet(
		paths = "/bin/testProject/compListServlet",
		methods="GET,POST"
)
public class ComponentListServlet extends SlingSafeMethodsServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Reference
	ComponentListUtil util;
	
	@Override
    protected void doGet(final SlingHttpServletRequest req,
            final SlingHttpServletResponse resp) throws ServletException, IOException {
        System.out.println(req.getQueryString());
		String componentName = req.getParameter("compName");
        if(componentName==null)
        {
        	return;
        }
        Map<String,String> usage = util.getUsage(componentName);
       
        JSONObject jsonResponse = new JSONObject();
        JSONArray pagesLinks = new JSONArray();
        
        JSONObject page = null;
        try
        {
	        for(Map.Entry<String, String> entry:usage.entrySet())
	        {
	        	page = new JSONObject();
	        	page.put("name", entry.getKey());
	        	page.put("path", entry.getValue());
	        	pagesLinks.put(page);
	        }
	        jsonResponse.put("pages", pagesLinks);
        }
        catch(JSONException jSEx)
        {
        	jSEx.printStackTrace();
        }
        resp.setContentType("application/json");
        resp.getWriter().write(jsonResponse.toString());
	}
	
    protected void doPost(final SlingHttpServletRequest req,
            final SlingHttpServletResponse resp) throws ServletException, IOException {
		doGet(req,resp);
	}
}
