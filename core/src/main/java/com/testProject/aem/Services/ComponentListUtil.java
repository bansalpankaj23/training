package com.testProject.aem.Services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

import com.day.cq.search.result.Hit;

@Component(immediate = true, metatype = false)
@Service(ComponentListUtil.class)
public class ComponentListUtil {

	@Reference
	ResourceResolverFactory resolverFactory;

	@Reference
	ComponentListService compListService;

	@Reference
	QueryBuilderUtil qBUtil;

	/** Names and Paths of components */
	private Map<String, String> components;

	/** Getter for the components list */
	public Map<String, String> getComponents() {
		populateComponents();
		return components;
	}

	public Map<String, String> getUsage(String componentName) {
		String componentPath = components.get(componentName);
		Map<String, String> pagesLinks = new HashMap<String, String>();

		usageUtil(componentPath, pagesLinks);

		ArrayList<String> tempPathStrings = new ArrayList<String>(Arrays.asList((componentPath.split("/"))));
		if (tempPathStrings.get(0).equals("")) {
			tempPathStrings.remove(0);
		}

		if (tempPathStrings.get(0).equals("apps") || tempPathStrings.get(0).equals("libs")) {
			tempPathStrings.remove(0);
		}
		String relComponentPath = String.join("/", tempPathStrings);

		usageUtil(relComponentPath, pagesLinks);
		return pagesLinks;
	}

	@SuppressWarnings("deprecation")
	private void usageUtil(String componentPath, Map<String, String> pagesLinks) {
		Iterator<Hit> hits;
		Map<String, String> predicateMap = new HashMap<String, String>();

		try {
			predicateMap.put("path", compListService.getWebsitePath());
			predicateMap.put("property", "sling:resourceType");
			predicateMap.put("property.value", componentPath);
			predicateMap.put("p.limit", "-1");
			hits = qBUtil.getResult(predicateMap).getHits().iterator();

			Hit tempHit;
			String[] pathStrings;
			ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			while (hits.hasNext()) {
				tempHit = hits.next();
				pathStrings = tempHit.getPath().split("/");
				int i = 0;
				for (; (i < pathStrings.length) && (!pathStrings[i].equals("jcr:content")); ++i)
					;
				StringBuffer strBuffer = new StringBuffer();

				for (int j = 1; j < i; ++j) {
					strBuffer.append('/');
					strBuffer.append(pathStrings[j]);
				}

				String currentPath = strBuffer.toString();
				Node pageProperties = resourceResolver.getResource(currentPath).getChild("jcr:content")
						.adaptTo(Node.class);
				String currentTitle = pageProperties.getProperty("jcr:title").getString();

				pagesLinks.put(currentTitle, currentPath);
			}

			return;
		} catch (NullPointerException npEx) {
			npEx.printStackTrace();
		} catch (RepositoryException rEx) {
			rEx.printStackTrace();
		} catch (LoginException lEx) {
			lEx.printStackTrace();
		}

	}

	/* Refresh the components list */
	private void populateComponents() {
		Map<String, String> predicateMap = new HashMap<String, String>();
		String project = compListService.getProjectPath();
		predicateMap.put("path", project);
		predicateMap.put("type", "cq:Component");
		predicateMap.put("p.limit", "-1");
		try {
			Iterator<Node> result = qBUtil.getResult(predicateMap).getNodes();
			Node temp;

			components.clear();

			while (result.hasNext()) {
				temp = result.next();
				components.put(temp.getProperty("jcr:title").getString(), temp.getPath());
			}
		} catch (RepositoryException rEx) {
			rEx.printStackTrace();
		}
	}

	@Activate
	protected void activate() {
		components = new TreeMap<String, String>();
		populateComponents();
	}
}
