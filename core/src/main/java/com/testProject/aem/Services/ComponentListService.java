package com.testProject.aem.Services;

import java.util.Dictionary;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.oak.commons.PropertiesUtil;
import org.osgi.service.component.ComponentContext;

@Component(immediate = true, metatype = true)
@Service(ComponentListService.class)
public class ComponentListService {

	@Property(label = "Project path", description = "The path of the project whose components you want to see", value = "/apps/bhf")
	private static final String PROJECT_PATH = "project.path";

	@Property(label = "Website path", description = "The path of the website whose components you want to see", value = "/content/bhf")
	private static final String WEBSITE_PATH = "website.path";

	/** The value of the Project Path property */
	private String projectPath;

	/** The value of the Website Path property */
	private String websitePath;

	public String getProjectPath() {
		return projectPath;
	}

	public String getWebsitePath() {
		return websitePath;
	}

	@Activate
	protected void activate(ComponentContext context) {
		@SuppressWarnings("rawtypes")
		Dictionary dictionary = context.getProperties();
		setComponentListServiceProperties(dictionary.get(PROJECT_PATH), dictionary.get(WEBSITE_PATH));
	}

	@Modified
	protected void modified(ComponentContext context) {
		@SuppressWarnings("rawtypes")
		Dictionary dictionary = context.getProperties();
		setComponentListServiceProperties(dictionary.get(PROJECT_PATH), dictionary.get(WEBSITE_PATH));
	}

	private void setComponentListServiceProperties(Object projectPath, Object websitePath) {
		this.projectPath = PropertiesUtil.toString(projectPath, "");
		this.websitePath = PropertiesUtil.toString(websitePath, "");

	}
}