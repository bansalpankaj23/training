package com.testProject.aem.Services;

import java.util.Map;

import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;

@Component(immediate = true, metatype = false)
@Service (QueryBuilderUtil.class)
public class QueryBuilderUtil {
	
	@Reference
	QueryBuilder builder;
	
	@Reference
	ResourceResolverFactory resolverFactory;
	
	@SuppressWarnings("rawtypes")
	public SearchResult getResult(Map map)
	{
		ResourceResolver resourceResolver;
		Session session;
		try {
			resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			session =  resourceResolver.adaptTo(Session.class);
			Query query = builder.createQuery(PredicateGroup.create(map), session);
			SearchResult result = query.getResult();
			
			return result;
		} 
		catch (LoginException lEx) {
		}	
		return null;
	}
}
