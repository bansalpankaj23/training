package com.testProject.aem.models;

import java.util.Iterator;

import com.adobe.cq.sightly.WCMUsePojo;
import com.testProject.aem.Services.ComponentListUtil;

public class ComponentListModel extends WCMUsePojo {

	ComponentListUtil util;

	Iterator<String> comps;

	String name = "Anirudh";

	@Override
	public void activate() {
		// TODO Auto-generated method stub
		util = getSlingScriptHelper().getService(ComponentListUtil.class);
	}

	public Iterator<String> getComps() {
		comps = util.getComponents().keySet().iterator();
		return comps;
	}

	public String getName() {
		return name;
	}
}
