**compReportComponent**
	1. calls the name getter through ComponentListModel, receives iterator, puts the options in a drop-down
	2. passes name of selected component to servlet, receives AJAX response and displays
~~~
__parallel to ComponentListModel__
**componentListServlet**
	Passes the received name to componentListUtil, parses result, puts in JSON and returns to front end
~~~
**ComponentListModel**
	Calls the Util, returns iterator of component names

**ComponentListUtil**
	Gets config, builds predicate map, passes it to QueryBuilderUtil, strips unnecessary data from result, returns and saves it
	
**ComponentListService**
	DAO for config

**QueryBuilderUtil**
	Receives a predicate map, builds and executes the query, returns the result object
